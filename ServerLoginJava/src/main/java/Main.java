import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.Properties;

public class Main {
    public static void main(String[] args) {

        Properties serverProps = new Properties();
        FileInputStream input = null;
        try {
            input = new FileInputStream("src/main/resources/Config/config.properties");
            serverProps.load(input);
            HandleSocket handleSocket = new HandleSocket(serverProps,"staff");
            handleSocket.reciveData();
        } catch (FileNotFoundException e) {
            e.printStackTrace();

        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}
