import Utils.CreateConnection;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.Properties;

public class HandleAccount {
    private CreateConnection newConn;
    private String tableName;
    public HandleAccount(Properties props, String tableName)
    {
        this.newConn = new CreateConnection(props);
        this.tableName=tableName;
    }

    public int findIdStaff(String user, String password) {
        Connection connection=null;
        PreparedStatement preparedStatement = null;
        try{
            connection=newConn.getConnection();
            String query = String.format("SELECT * FROM %s where user=? and password=?",tableName);
            preparedStatement = connection.prepareStatement(query);

            preparedStatement.setString(1,user);
            preparedStatement.setString(2,password);

            ResultSet result = preparedStatement.executeQuery();
            if(result.next())
                return result.getInt(1);
        }
        catch (Exception ex)
        {
            throw new IllegalArgumentException(ex.getMessage());
        }
        finally {
            try {
                if (connection!=null || !connection.isClosed())
                    connection.close();
            } catch (SQLException e) {
                throw new IllegalArgumentException(e.getMessage());
            }
        }
        return -1;
    }
}
