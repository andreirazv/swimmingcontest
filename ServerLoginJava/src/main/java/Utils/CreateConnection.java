package Utils;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.util.Properties;

public class CreateConnection {
    private Properties prop;
    public CreateConnection(Properties props){
        this.prop=props;
    }

    private Connection instance=null;

    private Connection createNewConnection() throws SQLException {

        String userName;
        String password;
        String url;
        String driver;

        Connection conn = null;
        //Properties prop = new Properties();

        try {
            //input = new FileInputStream("src/main/resources/Config/config.properties");
            //prop.load(input);
            userName=prop.getProperty("userName");
            password=prop.getProperty("password");
            url=prop.getProperty("url");
            driver=prop.getProperty("driver");

            Class.forName(driver);
            if (userName!=null && password!=null)
                conn= DriverManager.getConnection(url,userName,password);
            else
                conn=DriverManager.getConnection(url);

            System.out.println("Connected to database");
        } catch (ClassNotFoundException ex) {
            System.out.println(ex.getMessage());
        }
        return conn;
    }
    public Connection getConnection(){
        // logger.traceEntry();
        try {
            if (instance==null || instance.isClosed())
                instance=createNewConnection();

        } catch (SQLException e) {
            //logger.error(e);
            System.out.println("Error DB "+e);
        }
        //  logger.traceExit(instance);
        return instance;
    }
}
