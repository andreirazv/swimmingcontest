import Utils.CreateConnection;

import java.io.*;
import java.net.ServerSocket;
import java.net.Socket;
import java.util.Date;
import java.util.Properties;
import java.util.Scanner;

public class HandleSocket {
    private Properties props;
    private String tableName;

    HandleSocket(Properties props, String tableName) {
        this.props = props;
        this.tableName = tableName;
    }

    private static final int PORT = 25000;


    private static Socket socket;

    void reciveData() {
        HandleAccount handleAccount = new HandleAccount(props, tableName);

        try {

            ServerSocket serverSocket = new ServerSocket(PORT);
            System.out.println("Server Started");

            while (true) {
                socket = serverSocket.accept();
                InputStream is = socket.getInputStream();
                InputStreamReader isr = new InputStreamReader(is);
                BufferedReader br = new BufferedReader(isr);
                String username = br.readLine();
                String password = br.readLine();
                System.out.println("Message received from client is " + username);
                int rez=handleAccount.findIdStaff(username,password);
                String returnMessage=rez+"\n";
                OutputStream os = socket.getOutputStream();
                OutputStreamWriter osw = new OutputStreamWriter(os);
                BufferedWriter bw = new BufferedWriter(osw);
                bw.write(returnMessage);
                System.out.println("Message sent to the client is " + returnMessage);
                bw.flush();
            }
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            try {
                socket.close();
            } catch (Exception e) {
            }
        }
    }

}