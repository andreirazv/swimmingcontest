package Service;

import Domain.Participant;
import Repository.DBParticipant;
import Repository.DBProba;
import Service.Interface.IParticipantService;

public class ParticipantService implements IParticipantService {

    private DBParticipant dbParticipant;

    public ParticipantService(DBParticipant dbParticipant)
    {
        this.dbParticipant=dbParticipant;
    }
    @Override
    public Iterable<Participant> getAllByProba(int id){
        return dbParticipant.getAllByProba(id);
    }
    @Override
    public Iterable<Participant> getAll(){
        return dbParticipant.getAll();
    }

    @Override
    public Iterable<Participant> getParticipantByProbaId(int id) {
        return dbParticipant.getParticipantByProbaId(id);
    }

    @Override
    public int addParticipant(String firstName, String lastName, int age) {
        Participant participant = new Participant(firstName,lastName,age);
        return dbParticipant.addParticipant(participant);
    }
}
