package Service;

import Domain.Proba;
import Domain.Style;
import Repository.DBProba;
import Service.Interface.IProbaService;

public class ProbaService implements IProbaService {

    private DBProba dbProba;

    public ProbaService(DBProba dbProba)
    {
        this.dbProba=dbProba;
    }
    @Override
    public Iterable<Proba> getAll()
    {
        return dbProba.getAll();
    }
    @Override
    public Iterable<String> getAllByIdParticipant(int id)
    {
        return dbProba.getAllByIdParticipant(id);
    }

    @Override
    public Proba findProba(Style style, float distance) {
        return dbProba.findProba(style,distance);
    }
}
