package Service;

import Domain.Staff;
import Repository.DBStaff;
import Service.Interface.IStaffService;

import java.io.*;
import java.net.InetAddress;
import java.net.Socket;
import java.util.NoSuchElementException;
import java.util.Scanner;

import static Utils.Utils.hashMd5;

public class StaffService implements IStaffService {

    private DBStaff dbStaff;

    public StaffService(DBStaff dbStaff)
    {
        this.dbStaff=dbStaff;
    }
    private static InetAddress host;
    private static final int PORT = 1234;

    private static Socket socket;
    @Override
    public Staff findStaffById(int id)
    {
        Staff staff = dbStaff.findStaffById(id);
        if(staff==null)
            throw new IllegalArgumentException("Staff was not found");
        return staff;
    }
    @Override
    public Staff checkLoginWithSocket(String userName, String password)
    {
        String hashPass = hashMd5(password);
        try
        {
            String host = "localhost";
            int port = 25000;
            InetAddress address = InetAddress.getByName(host);
            socket = new Socket(address, port);

            OutputStream os = socket.getOutputStream();
            OutputStreamWriter osw = new OutputStreamWriter(os);
            BufferedWriter bw = new BufferedWriter(osw);

            String sendMessage = userName+ "\n";;
            hashPass=hashPass + "\n";
            bw.write(sendMessage);
            bw.write(hashPass);
            bw.flush();
            System.out.println("Message sent to the server : "+sendMessage);

            //Get the return message from the server
            InputStream is = socket.getInputStream();
            InputStreamReader isr = new InputStreamReader(is);
            BufferedReader br = new BufferedReader(isr);
            String message = br.readLine();
            System.out.println("Message received from the server : " +message);


            Staff staff = this.findStaffById(Integer.parseInt(message));
            staff.setPassword(null);

            return staff;
        }
        catch (Exception exception)
        {
            System.out.println(exception.getMessage());
        }
        finally
        {
            try
            {
                socket.close();
            }
            catch(Exception e)
            {
                e.printStackTrace();
            }
        }
        return null;
    }
    @Override
    public Staff checkLogin(String userName, String password)
    {
        try
        {
            String hashPass = hashMd5(password);
            Staff staff = dbStaff.findStaff(userName,hashPass);
            staff.setPassword(null);

            return staff;
        }
        catch (Exception exception)
        {
            System.out.println(exception.getMessage());
        }

        return null;
    }
}
