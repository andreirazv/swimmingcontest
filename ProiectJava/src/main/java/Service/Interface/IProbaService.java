package Service.Interface;

import Domain.Style;

public interface IProbaService<Proba> {
    Iterable<Proba> getAll();
    Iterable<String> getAllByIdParticipant(int id);
    Proba findProba(Style style, float distance);
}
