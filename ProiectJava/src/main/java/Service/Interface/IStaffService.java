package Service.Interface;

public interface IStaffService<Staff> {

    Staff findStaffById(int id);
    Staff checkLogin(String userName, String password);
    Staff checkLoginWithSocket(String userName, String password);
}
