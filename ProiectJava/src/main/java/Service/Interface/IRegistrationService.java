package Service.Interface;

public interface IRegistrationService<Registration> {

    int getParticipantCount(int idProba);
    void registerParticipant(int idParticipant,int idProba);
}
