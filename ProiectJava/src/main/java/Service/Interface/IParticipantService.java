package Service.Interface;

import Domain.Participant;

public interface IParticipantService<Participant> {
    Iterable<Participant> getAllByProba(int id);
    Iterable<Participant> getAll();
    Iterable<Participant> getParticipantByProbaId(int id);
    int addParticipant(String firstName,String lastName,int age);
}
