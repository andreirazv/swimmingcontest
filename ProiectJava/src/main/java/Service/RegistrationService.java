package Service;

import Repository.DBRegistration;
import Service.Interface.IRegistrationService;

public class RegistrationService implements IRegistrationService {
    private DBRegistration dbRegistration;
    public RegistrationService(DBRegistration dbRegistration)
    {
        this.dbRegistration=dbRegistration;
    }
    @Override
    public int getParticipantCount(int idProba)
    {
        int count=dbRegistration.getParticipantCount(idProba);
        if(count!=-1)
            return count;
        else
            throw new IllegalArgumentException("(1) - Eroare!");
    }
    public void registerParticipant(int idParticipant,int idProba)
    {
        dbRegistration.registerParticipant(idParticipant,idProba);
    }
}
