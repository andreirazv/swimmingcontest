package Repository;

import Domain.Proba;
import Domain.Style;
import Repository.Interface.ProbaRepository;
import Utils.CreateConnection;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import java.util.Properties;

public class DBProba implements ProbaRepository<Integer, Proba> {
    private static Logger logger = (Logger) LogManager.getLogger(DBProba.class.getName());
    private CreateConnection newConn;
    private String tableName;
    public DBProba(Properties props, String tableName)
    {
        this.newConn = new CreateConnection(props);
        this.tableName=tableName;
    }

    private Proba createObject(ResultSet result) {
        try {

            int id = result.getInt(1);
            Style style=Style.valueOf(result.getString(2));
            float distance=result.getFloat(3);
            return new Proba(id,style,distance);
        } catch (SQLException e) {
            throw new IllegalArgumentException(e.getMessage());
        }
    }

    @Override
    public Iterable <Proba> getAll() {
        Connection connection=null;
        logger.traceEntry("Messages");
        logger.traceExit();
        PreparedStatement preparedStatement = null;
        try{
            connection=newConn.getConnection();
            String query = String.format("SELECT * FROM %s",tableName);
            preparedStatement = connection.prepareStatement(query);

            ResultSet result = preparedStatement.executeQuery();
            List<Proba> allItem=new ArrayList <>();
            while(result.next())
                allItem.add(this.createObject(result));
            return allItem;
        }
        catch (Exception ex)
        {
            throw new IllegalArgumentException(ex.getMessage());
        }
        finally {
            try {
                if (connection!=null || !connection.isClosed())
                    connection.close();
            } catch (SQLException e) {
                throw new IllegalArgumentException(e.getMessage());
            }
        }
    }

    @Override
    public Iterable <String> getAllByIdParticipant(int id) {
        Connection connection=null;
        logger.traceEntry("Messages");
        logger.traceExit();
        PreparedStatement preparedStatement = null;
        try{
            connection=newConn.getConnection();
            String query = String.format("SELECT %1$s.id,%1$s.style,%1$s.distance from %1$s INNER JOIN registration where registration.idProba=%1$s.id and registration.idParticipant=?;",tableName);
            preparedStatement = connection.prepareStatement(query);
            preparedStatement.setInt(1,id);
            ResultSet result = preparedStatement.executeQuery();
            List<String> allItem=new ArrayList <>();
            while(result.next())
                allItem.add(this.createObject(result).getStyle()+" - "+this.createObject(result).getDistance());
            return allItem;
        }
        catch (Exception ex)
        {
            throw new IllegalArgumentException(ex.getMessage());
        }
        finally {
            try {
                if (connection!=null || !connection.isClosed())
                    connection.close();
            } catch (SQLException e) {
                throw new IllegalArgumentException(e.getMessage());
            }
        }
    }

    @Override
    public Proba findProba(Style style, float distance) {
        Connection connection=null;
        logger.traceEntry("Messages");
        logger.traceExit();
        PreparedStatement preparedStatement = null;
        try{
            connection=newConn.getConnection();
            String query = String.format("SELECT * from %1$s where style=? and distance=?;",tableName);
            preparedStatement = connection.prepareStatement(query);
            preparedStatement.setString(1, String.valueOf(style));
            preparedStatement.setFloat(2,distance);
            ResultSet result = preparedStatement.executeQuery();
            if(result.next())
               return this.createObject(result);
        }
        catch (Exception ex)
        {
            throw new IllegalArgumentException(ex.getMessage());
        }
        finally {
            try {
                if (connection!=null || !connection.isClosed())
                    connection.close();
            } catch (SQLException e) {
                throw new IllegalArgumentException(e.getMessage());
            }
        }
        return null;
    }

}
