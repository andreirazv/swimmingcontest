package Repository;

import Domain.Staff;
import Repository.Interface.StaffRepository;
import Utils.CreateConnection;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.Properties;

import java.sql.Connection;

public class DBStaff implements StaffRepository<Integer, Staff> {
    private static Logger logger = LogManager.getLogger(DBStaff.class.getName());
    private CreateConnection newConn;
    private String tableName;
    public DBStaff(Properties props,String tableName)
    {
        this.newConn = new CreateConnection(props);
        this.tableName=tableName;
    }

    private Staff createObject(ResultSet result) {
        try {
            int id = result.getInt(1);
            String user=result.getString(2);
            String password=result.getString(3);
            String firstName=result.getString(4);
            String lastName=result.getString(5);
            String email=result.getString(6);
            return new Staff(id,user,password,firstName,lastName,email);
        } catch (SQLException e) {
            throw new IllegalArgumentException(e.getMessage());
        }
    }

    @Override
    public Staff findStaff(String user, String password) {
        Connection connection=null;
        logger.traceEntry("Messages");
        logger.traceExit();
        PreparedStatement preparedStatement = null;
        try{
            connection=newConn.getConnection();
            String query = String.format("SELECT * FROM %s where user=? and password=?",tableName);
            preparedStatement = connection.prepareStatement(query);

            preparedStatement.setString(1,user);
            preparedStatement.setString(2,password);

            ResultSet result = preparedStatement.executeQuery();
            if(result.next())
                return this.createObject(result);
        }
        catch (Exception ex)
        {
            throw new IllegalArgumentException(ex.getMessage());
        }
        finally {
            try {
                if (connection!=null || !connection.isClosed())
                connection.close();
            } catch (SQLException e) {
                throw new IllegalArgumentException(e.getMessage());
            }
        }
        return null;
    }

    @Override
    public Staff findStaffById(int id) {
        Connection connection=null;
        logger.traceEntry("Messages");
        logger.traceExit();
        PreparedStatement preparedStatement = null;
        try{
            connection=newConn.getConnection();
            String query = String.format("SELECT * FROM %s where id=?",tableName);
            preparedStatement = connection.prepareStatement(query);

            preparedStatement.setInt(1,id);

            ResultSet result = preparedStatement.executeQuery();
            if(result.next())
                return this.createObject(result);
        }
        catch (Exception ex)
        {
            throw new IllegalArgumentException(ex.getMessage());
        }
        finally {
            try {
                if (connection!=null || !connection.isClosed())
                    connection.close();
            } catch (SQLException e) {
                throw new IllegalArgumentException(e.getMessage());
            }
        }
        return null;
    }
}
