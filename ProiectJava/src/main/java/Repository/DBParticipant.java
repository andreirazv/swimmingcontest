package Repository;


import Domain.Participant;
import Domain.Proba;
import Domain.Style;
import Repository.Interface.ParticipantRepository;
import Utils.CreateConnection;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.sql.*;
import java.util.ArrayList;
import java.util.List;
import java.util.Properties;

public class DBParticipant implements ParticipantRepository<Integer, Participant> {
    private static Logger logger = (Logger) LogManager.getLogger(DBParticipant.class.getName());

    private CreateConnection newConn;
    private String tableName;
    public DBParticipant(Properties props, String tableName)
    {
        this.newConn = new CreateConnection(props);
        this.tableName=tableName;
    }

    private Participant createObject(ResultSet result) {
        try {
            int id = result.getInt(1);
            String firstName=result.getString(2);
            String lastName=result.getString(3);
            int age=result.getInt(4);
            return new Participant(id,firstName,lastName,age);
        } catch (SQLException e) {
            throw new IllegalArgumentException(e.getMessage());
        }
    }
    @Override
    public Iterable <Participant> getAll() {
        Connection connection=null;
        logger.traceEntry("Messages");
        logger.traceExit();
        PreparedStatement preparedStatement = null;
        try{
            connection=newConn.getConnection();
            String query = String.format("SELECT * FROM %s",tableName);
            preparedStatement = connection.prepareStatement(query);

            ResultSet result = preparedStatement.executeQuery();
            List<Participant> allItem=new ArrayList <>();
            while(result.next())
                allItem.add(this.createObject(result));
            return allItem;
        }
        catch (Exception ex)
        {
            throw new IllegalArgumentException(ex.getMessage());
        }
        finally {
            try {
                if (connection!=null || !connection.isClosed())
                    connection.close();
            } catch (SQLException e) {
                throw new IllegalArgumentException(e.getMessage());
            }
        }
    }

    @Override
    public Iterable <Participant> getParticipantByProbaId(int id) {
        Connection connection=null;
        logger.traceEntry("Messages");
        logger.traceExit();
        PreparedStatement preparedStatement = null;
        try{
            connection=newConn.getConnection();
            String query = String.format("SELECT %1$s.id,%1$s.firstName,%1$s.lastName,%1$s.age FROM %1$s inner join registration where %1$s.id = registration.idParticipant and registration.idProba=?;",tableName);
            preparedStatement = connection.prepareStatement(query);
            preparedStatement.setInt(1,id);
            ResultSet result = preparedStatement.executeQuery();
            List<Participant> allItem=new ArrayList <>();
            while(result.next())
                allItem.add(this.createObject(result));
            return allItem;
        }
        catch (Exception ex)
        {
            throw new IllegalArgumentException(ex.getMessage());
        }
        finally {
            try {
                if (connection!=null || !connection.isClosed())
                    connection.close();
            } catch (SQLException e) {
                throw new IllegalArgumentException(e.getMessage());
            }
        }
    }

    @Override
    public Iterable <Participant> getAllByProba(int id) {
        Connection connection=null;
        logger.traceEntry("Messages");
        logger.traceExit();
        PreparedStatement preparedStatement = null;
        try{
            connection=newConn.getConnection();
            String query = String.format(
            "SELECT %1$s.id,%1$s.firstName,%1$s.lastName,%1$s.age FROM %1$s inner join registration ON %1$s.id = registration.idParticipant and registration.idProba=?;"
                    ,tableName);
            preparedStatement = connection.prepareStatement(query);
            preparedStatement.setInt(1,id);
            ResultSet result = preparedStatement.executeQuery();
            List<Participant> allItem=new ArrayList<>();
            while(result.next())
                allItem.add(this.createObject(result));
            return allItem;
        }
        catch (Exception ex)
        {
            throw new IllegalArgumentException(ex.getMessage());
        }
        finally {
            try {
                if (connection!=null || !connection.isClosed())
                    connection.close();
            } catch (SQLException e) {
                throw new IllegalArgumentException(e.getMessage());
            }
        }
    }

    @Override
    public int addParticipant(Participant participant) {
        Connection connection=null;
        logger.traceEntry("Messages");
        logger.traceExit();
        PreparedStatement preparedStatement = null;
        try{
            connection=newConn.getConnection();
            String query = String.format("INSERT INTO %s (firstName,lastName,age) VALUES(?, ?, ?);",tableName);
            preparedStatement = connection.prepareStatement(query,Statement.RETURN_GENERATED_KEYS);
            preparedStatement.setString(1,participant.getFirstName());
            preparedStatement.setString(2,participant.getLastName());
            preparedStatement.setInt(3,participant.getAge());

             preparedStatement.execute();
            ResultSet result = preparedStatement.getGeneratedKeys();
            if(result.next())
                return result.getInt(1);
        }
        catch (Exception ex)
        {
            throw new IllegalArgumentException(ex.getMessage());
        }
        finally {
            try {
                if (connection!=null || !connection.isClosed())
                    connection.close();
            } catch (SQLException e) {
                throw new IllegalArgumentException(e.getMessage());
            }
        }
        return -1;
    }

}
