package Repository.Interface;

import Domain.Style;

public interface ProbaRepository<Integer,Proba> {
    Iterable<Proba> getAll();
    Iterable<String> getAllByIdParticipant(int id);
    Proba findProba(Style style, float distance);
}
