package Repository.Interface;

public interface RegistrationRepository<Integer,Registration> {
    int getParticipantCount(int idProba);
    void registerParticipant(int idParticipant,int idProba);
}
