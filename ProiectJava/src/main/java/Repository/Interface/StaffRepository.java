package Repository.Interface;

public interface StaffRepository<Integer,Staff> {
    Staff findStaff(String user,String password);
    Staff findStaffById(int id);
}
