package Repository.Interface;

import Domain.Participant;

public interface ParticipantRepository<Integer,Participant> {

    Iterable<Participant> getAllByProba(int id);
    int addParticipant(Participant participant);
    Iterable <Participant> getAll();
    Iterable <Participant> getParticipantByProbaId(int id);
}
