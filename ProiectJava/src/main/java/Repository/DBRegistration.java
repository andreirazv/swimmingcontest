package Repository;

import Domain.Registration;
import Repository.Interface.RegistrationRepository;
import Utils.CreateConnection;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.sql.*;
import java.util.Properties;

public class DBRegistration implements RegistrationRepository<Integer, Registration> {
    private static Logger logger = (Logger) LogManager.getLogger(DBRegistration.class.getName());
    private CreateConnection newConn;
    private String tableName;
    public DBRegistration(Properties props, String tableName)
    {
        this.newConn = new CreateConnection(props);
        this.tableName=tableName;
    }

    @Override
    public int getParticipantCount(int idProba) {
        Connection connect=null;
        try {
            connect = newConn.getConnection();
            String query = String.format("SELECT count(*) FROM %s where idProba=?",tableName);
            PreparedStatement preparedStatement = connect.prepareStatement(query);
            preparedStatement.setInt(1,idProba);
            ResultSet resultSet = preparedStatement.executeQuery();

            if (resultSet.next()) {
                return resultSet.getInt(1);
            }
        } catch (Exception e) {
            throw new IllegalArgumentException(e.getMessage());
        }
        finally {
            try {
                if (connect!=null||!connect.isClosed())
                    connect.close();
            } catch (SQLException e) {
                throw new IllegalArgumentException(e.getMessage());
            }
        }
        return -1;
    }

    public void registerParticipant(int idParticipant,int idProba)
    {
        Connection connection=null;
        logger.traceEntry("Messages");
        logger.traceExit();
        PreparedStatement preparedStatement = null;
        try{
            connection=newConn.getConnection();
            String query = String.format("INSERT INTO %s (idParticipant,idProba) VALUES(?, ?);",tableName);
            preparedStatement = connection.prepareStatement(query);
            preparedStatement.setInt(1,idParticipant);
            preparedStatement.setInt(2,idProba);

            preparedStatement.execute();

        }
        catch (Exception ex)
        {
            throw new IllegalArgumentException(ex.getMessage());
        }
        finally {
            try {
                if (connection!=null || !connection.isClosed())
                    connection.close();
            } catch (SQLException e) {
                throw new IllegalArgumentException(e.getMessage());
            }
        }
    }

}
