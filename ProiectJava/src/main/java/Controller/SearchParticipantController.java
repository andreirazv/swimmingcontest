package Controller;

import Domain.Participant;
import Domain.Proba;
import Domain.Style;
import Service.ParticipantService;
import Service.ProbaService;
import javafx.beans.property.StringProperty;
import javafx.beans.binding.Bindings;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.scene.Node;
import javafx.scene.control.ComboBox;
import javafx.scene.control.TableCell;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.cell.PropertyValueFactory;

import java.util.stream.Collectors;
import java.util.stream.StreamSupport;

public class SearchParticipantController {
    public TableView <Participant> tViewParticipant;
    public TableColumn<String,Participant> collumnFistName;
    public TableColumn<String,Participant> collumnLastName;
    public TableColumn<Integer,Participant> collumnAge;
    public TableColumn<TableViewComboBox, StringProperty> collumnProba;

    public ComboBox<String> comboBoxProba;

    private ProbaService probaService;
    private ParticipantService participantService;

    private ObservableList<Participant> model = FXCollections.observableArrayList();

    void setInfo(ProbaService probaService,ParticipantService participantService)
    {
        this.participantService=participantService;
        this.probaService=probaService;
        init();
        listen();
    }
    private void init(){

        collumnFistName.setCellValueFactory(new PropertyValueFactory<>("firstName"));
        collumnLastName.setCellValueFactory(new PropertyValueFactory<>("lastName"));
        collumnAge.setCellValueFactory(new PropertyValueFactory <>("age"));
        collumnProba.setCellValueFactory(new PropertyValueFactory <>("idProperty"));

        initComboSearch();
        updateTable(-1);
    }
    private void initComboSearch(){
        ObservableList<String> options = FXCollections.observableArrayList();
        options.add("No selection");
        probaService.getAll().forEach(x->options.add(x.getStyle()+" - "+x.getDistance()));
        comboBoxProba.setItems(options);
        comboBoxProba.getSelectionModel().selectFirst();
    }
    private void listen(){
        comboBoxProba.valueProperty().addListener((ov, t, t1) -> {
            if (t1.equals("No selection")) {
                updateTable(-1);
            } else {
                String rez[] = t1.split(" - ");
                Proba proba = probaService.findProba(Style.valueOf(rez[0]), Float.parseFloat(rez[1]));
                updateTable(proba.getID());
                System.out.println(t1);
            }
        });


        collumnProba.setCellFactory(col -> {
            TableCell<TableViewComboBox, StringProperty> c = new TableCell<>();
            ComboBox<String> comboBox = new ComboBox<>();
            c.itemProperty().addListener((observable, oldValue, newValue) -> {
                if (newValue != null) {
                    int id = Integer.parseInt(newValue.get());
                    ObservableList<String> options = FXCollections.observableArrayList();
                    options.setAll(StreamSupport.stream(probaService.getAllByIdParticipant(id).spliterator(), false)
                            .collect(Collectors.toList()));
                    comboBox.setItems(options);
                    comboBox.getSelectionModel().selectFirst();
                }
            });
            c.graphicProperty().bind(Bindings.when(c.emptyProperty()).then((Node) null).otherwise(comboBox));
            return c;
        });

    }
    private void updateTable(int id)
    {
        model.clear();
        if(id==-1)
        {
            model.setAll(StreamSupport.stream(participantService.getAll().spliterator(), false)
                    .collect(Collectors.toList()));
        }
        else
        {
            model.setAll(StreamSupport.stream(participantService.getParticipantByProbaId(id).spliterator(), false)
                    .collect(Collectors.toList()));
        }
        tViewParticipant.setItems(model);
    }
}
