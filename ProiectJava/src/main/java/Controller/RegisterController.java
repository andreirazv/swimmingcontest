package Controller;

import Domain.Participant;
import Domain.Proba;
import Domain.Style;
import Service.ParticipantService;
import Service.ProbaService;
import Service.RegistrationService;
import com.google.gson.Gson;
import javafx.beans.property.SimpleObjectProperty;
import javafx.beans.value.ChangeListener;
import javafx.beans.value.ObservableValue;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.scene.control.CheckBox;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.TextField;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.util.Callback;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.StreamSupport;

public class RegisterController {

    public TableView<Proba> tViewSelectProba;
    public TextField textFieldFirstName;
    public TextField textFieldLastName;
    public TextField textFieldAge;
    public TableColumn<Style,Proba> collumnStyle;
    public TableColumn<Float,Proba> collumnDistance;
    public TableColumn<Integer, CheckBox> collumnSelect;

    private RegistrationService registrationService;
    private ProbaService probaService;
    private ParticipantService participantService;

    private List<Integer>registerProbe =new ArrayList<>();

    private ObservableList<Proba> model = FXCollections.observableArrayList();


    void setInfo(RegistrationService registrationService, ProbaService probaService, ParticipantService participantService)
    {
        this.participantService=participantService;
        this.registrationService=registrationService;
        this.probaService=probaService;
        init();
        listen();
    }
    private void init(){
        collumnStyle.setCellValueFactory(new PropertyValueFactory<>("style"));
        collumnDistance.setCellValueFactory(new PropertyValueFactory<>("distance"));
        collumnSelect.setCellValueFactory(new PropertyValueFactory<>("ID"));


        updateTable();
    }
    private void updateTable()
    {
        model.clear();
        model.setAll(StreamSupport.stream(probaService.getAll().spliterator(), false)
                .collect(Collectors.toList()));
        tViewSelectProba.setItems(model);
    }
    private void listen(){
        collumnSelect.setCellValueFactory(id -> {
            CheckBox checkBox = new CheckBox();
            Gson gson = new Gson();
            Proba p = gson.fromJson(String.valueOf(id.getValue()), Proba.class);
            checkBox.selectedProperty().setValue(false);
            checkBox.selectedProperty().addListener((ov, old_val, new_val) -> {
                if(new_val)
                    registerProbe.add(p.getID());
                if(!new_val)
                    registerProbe.remove(p.getID());
            });

            return new SimpleObjectProperty <>(checkBox);
        });
    }
    public void btnRegister()
    {
        if(!textFieldFirstName.getText().equals("") &&!textFieldLastName.getText().equals("")&&!textFieldAge.getText().equals("")&&registerProbe.size()!=0) {
            String firstName = textFieldFirstName.getText();
            String lastName = textFieldLastName.getText();
            int age = Integer.parseInt(textFieldAge.getText());
            int id = participantService.addParticipant(firstName, lastName, age);
            System.out.println("IDD:"+id);
            registerProbe.forEach(x -> registrationService.registerParticipant(id,x));
            registerProbe.clear();
        }
        else{
            System.out.println("You have to complete all data!");
        }
    }
}
