package Controller;

import Domain.Proba;
import Domain.Staff;
import Service.ParticipantService;
import Service.ProbaService;
import Service.RegistrationService;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.TableCell;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.stage.Stage;

import java.util.stream.Collectors;
import java.util.stream.StreamSupport;

public class StaffController {

    private Staff staff;
    private RegistrationService registrationService;
    private ProbaService probaService;
    private ParticipantService participantService;

    public TableColumn<Proba, String> collumnStyle;
    public TableColumn<Proba, Integer> collumnDistance;
    public TableColumn<Proba, Integer> collumnNRP;

    public TableView<Proba> tViewProba;

    private ObservableList<Proba> model = FXCollections.observableArrayList();

    void setInfo(RegistrationService registrationService, ProbaService probaService,ParticipantService participantService, Staff staff)
    {
        this.participantService=participantService;
        this.registrationService=registrationService;
        this.probaService=probaService;
        this.staff=staff;
        init();
        listen();
    }
    private void init(){

        collumnNRP.setCellValueFactory(new PropertyValueFactory <>("ID"));
        collumnDistance.setCellValueFactory(new PropertyValueFactory<>("Distance"));
        collumnStyle.setCellValueFactory(new PropertyValueFactory <>("Style"));

        updateTable();
    }
    private void updateTable()
    {
        model.clear();
        model.setAll(StreamSupport.stream(probaService.getAll().spliterator(), false)
                .collect(Collectors.toList()));
        tViewProba.setItems(model);
    }
    private void listen(){
        collumnNRP.setCellFactory(column -> new TableCell<Proba, Integer>() {
            @Override
            protected void updateItem(Integer item, boolean empty) {
                super.updateItem(item, empty);
                if (empty) {
                    setText(null);
                } else {
                    int count = registrationService.getParticipantCount(item);
                    setText(count + "");
                }
            }
        });
    }

    public void btnLogOut()
    {

    }
    public void btnSearchParticipant()
    {
        try {
            FXMLLoader fxmlLoader = new FXMLLoader(getClass().getResource("/View/searchParticipant.fxml"));
            Parent root = fxmlLoader.load();
            Stage stage = new Stage();
            stage.setTitle("Search Participant");
            stage.setScene(new Scene(root));
            SearchParticipantController controller = fxmlLoader.getController();
            controller.setInfo(probaService,participantService);
            stage.show();
        }
        catch (Exception ex)
        {
            System.out.println(ex.getMessage());
        }
    }
    public void btnRegisterParticipant()
    {
        try {
            FXMLLoader fxmlLoader = new FXMLLoader(getClass().getResource("/View/registerParticipant.fxml"));
            Parent root = fxmlLoader.load();
            Stage stage = new Stage();
            stage.setTitle("Register new Participant");
            stage.setScene(new Scene(root));
            RegisterController controller = fxmlLoader.getController();
            controller.setInfo(registrationService,probaService,participantService);
            stage.show();
        }
        catch (Exception ex)
        {
            System.out.println(ex.getMessage());
        }
    }
}
