package Controller;

import Domain.Staff;
import Service.ParticipantService;
import Service.ProbaService;
import Service.RegistrationService;
import Service.StaffService;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.TextField;
import javafx.stage.Stage;

public class LoginController {

    private StaffService staffService;
    private ParticipantService participantService;
    private ProbaService probaService;
    private RegistrationService registrationService;
    public TextField TextBoxUsername;
    public TextField TextBoxPassword;
    public void BtnLogInAction() {
        try {
           // Staff staff = staffService.checkLogin(TextBoxUsername.getText(), TextBoxPassword.getText());
            Staff staff = staffService.checkLoginWithSocket(TextBoxUsername.getText(), TextBoxPassword.getText());
            if(staff!=null) {
                FXMLLoader fxmlLoader = new FXMLLoader(getClass().getResource("/View/staff.fxml"));
                Parent root = fxmlLoader.load();
                Stage stage = new Stage();
                stage.setScene(new Scene(root));
                StaffController controller = fxmlLoader.getController();
                controller.setInfo(registrationService, probaService, participantService, staff);
                stage.show();
            }

        }
        catch (Exception ex)
        {
            System.out.println(ex.getMessage());
        }

    }
    public void HLinkForgotPassword()
    {

    }
    public void setService(StaffService staffService,ProbaService probaService,RegistrationService registrationService,ParticipantService participantService)
    {
        this.participantService=participantService;
        this.probaService=probaService;
        this.registrationService=registrationService;
        this.staffService=staffService;
    }
}
