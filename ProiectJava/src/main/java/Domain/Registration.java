package Domain;

public class Registration implements HasID<Integer> {
    private int id;
    private int idParticipant;
    private int idProba;

    public Registration(int id, int idParticipant, int idProba) {
        this.id = id;
        this.idParticipant = idParticipant;
        this.idProba = idProba;
    }

    public int getIdParticipant() {
        return idParticipant;
    }

    public int getIdProba() {
        return idProba;
    }

    @Override
    public Integer getID() {
        return id;
    }
}