package Domain;

public class Proba implements HasID<Integer> {
    private int id;
    private Style style;
    private float distance;

    public Proba(int id, Style style, float distance) {
        this.id = id;
        this.style = style;
        this.distance = distance;
    }
    //manuela.petrescu@gmail.com
    public Style getStyle() {
        return style;
    }

    public float getDistance() {
        return distance;
    }

    @Override
    public Integer getID() {
        return id;
    }

    @Override
    public String toString() {
        return "{" +
                "id=" + id +
                ", style=" + style +
                ", distance=" + distance +
                '}';
    }
}
