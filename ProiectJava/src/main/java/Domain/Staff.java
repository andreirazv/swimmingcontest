package Domain;

public class Staff implements HasID<Integer>{
    private int id;
    private String user;
    private String password;
    private String firstName;
    private String lastName;
    private String email;

    public Staff(int id, String user, String password, String firstName, String lastName, String email) {
        this.id = id;
        this.user = user;
        this.password = password;
        this.firstName = firstName;
        this.lastName = lastName;
        this.email = email;
    }

    public String getUser() {
        return user;
    }

    public String getPassword() {
        return password;
    }
    public void setPassword(String password)
    {
        this.password=password;
    }
    public String getFirstName() {
        return firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public String getEmail() {
        return email;
    }

    @Override
    public Integer getID() {
        return id;
    }
}
