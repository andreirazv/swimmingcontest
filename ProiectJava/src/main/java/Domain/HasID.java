package Domain;

public interface HasID<ID> {
    ID getID();
}
