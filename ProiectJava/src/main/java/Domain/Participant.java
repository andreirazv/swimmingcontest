package Domain;


import javafx.beans.property.SimpleStringProperty;
import javafx.beans.property.StringProperty;

public class Participant implements HasID<Integer> {
    private int id;
    private String firstName;
    private String lastName;
    private int age;

    public Participant(int id, String firstName, String lastName, int age) {
        this.id = id;
        this.firstName = firstName;
        this.lastName = lastName;
        this.age = age;
    }
    public Participant(String firstName, String lastName, int age) {
        this.firstName = firstName;
        this.lastName = lastName;
        this.age = age;
    }

    public String getFirstName() {
        return firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public int getAge() {
        return age;
    }

    public StringProperty getIdProperty(){
        SimpleStringProperty property = new SimpleStringProperty(id+"");
        return property;
    }

    @Override
    public Integer getID() {
        return id;
    }

    @Override
    public String toString() {
        return "Participant{" +
                "id=" + id +
                ", firstName='" + firstName + '\'' +
                ", lastName='" + lastName + '\'' +
                ", age=" + age +
                '}';
    }
}
