import Controller.LoginController;
import Repository.DBParticipant;
import Repository.DBProba;
import Repository.DBRegistration;
import Repository.DBStaff;
import Repository.Interface.StaffRepository;
import Service.ParticipantService;
import Service.ProbaService;
import Service.RegistrationService;
import Service.StaffService;
import Utils.CreateConnection;
import javafx.application.Application;
import javafx.fxml.FXMLLoader;
import javafx.scene.Scene;
import javafx.scene.layout.Pane;
import javafx.stage.Stage;

import java.io.FileInputStream;
import java.sql.Connection;
import java.util.Properties;


public class Main extends Application {


    @Override
    public void start(Stage primaryStage) throws Exception {

        Properties serverProps=new Properties();
        FileInputStream input = new FileInputStream("src/main/resources/Config/config.properties");
        serverProps.load(input);

        DBStaff dbStaff= new DBStaff(serverProps,"staff");
        StaffService staffService = new StaffService(dbStaff);

        DBProba dbProba= new DBProba(serverProps,"proba");
        ProbaService probaService = new ProbaService(dbProba);

        DBRegistration dbRegistration= new DBRegistration(serverProps,"registration");
        RegistrationService registrationService = new RegistrationService(dbRegistration);

        DBParticipant dbParticipant= new DBParticipant(serverProps,"participant");
        ParticipantService participantService = new ParticipantService(dbParticipant);

        FXMLLoader loader = new FXMLLoader();
        loader.setLocation(getClass().getResource("./View/login.fxml"));
        Pane rootLayout = loader.load();
        LoginController controller = loader.getController();
        Scene loginScene = new Scene(rootLayout);
        primaryStage.setScene(loginScene);
        controller.setService(staffService,probaService,registrationService,participantService);
       // controller.setPrimaryStage(servStud,servHome,servNota,primaryStage);
        primaryStage.setTitle("Login");
        primaryStage.show();
    }


    public static void main(String[] args) {
        launch(args);
    }
}