﻿using log4net.Config;
using ProiectCMpp.Domain;
using ProiectCMpp.Repository;
using ProiectCMpp.Service;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Diagnostics;
using System.Linq;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace ProiectCMpp
{
    static class Program
    {
        /// <summary>
        /// The main entry point for the application.
        /// </summary>
        [STAThread]
        static void Main()
        {
            XmlConfigurator.Configure(new System.IO.FileInfo("log4j.xml"));
            Console.WriteLine("Configuration Settings for db {0}", GetConnectionStringByName("configDB"));

             IDictionary<string, string> props = new SortedList<string, string>();
             props.Add("ConnectionString", GetConnectionStringByName("configDB"));

            /*DbProba repo = new DbProba(props);
            DbParticipant part = new DbParticipant(props);
            part.GetAll();
            foreach (Proba p in repo(1))
            {
                Debug.WriteLine(p.Id + " - " + p.Style + " - " + p.Distance);
            }*/

            DbParticipant dbParticipant = new DbParticipant(props);
            ParticipantService participantService=new ParticipantService(dbParticipant);

            DbProba dbProba = new DbProba(props);
            ProbaService probaService=new ProbaService(dbProba);

            DbRegistration dbRegistration = new DbRegistration(props);
            RegistrationService registrationService=new RegistrationService(dbRegistration);

            DbStaff dbStaff = new DbStaff(props);
            StaffService staffService = new StaffService(dbStaff);

            Application.EnableVisualStyles();
            Application.SetCompatibleTextRenderingDefault(false);
            Application.Run(new LoginView( props,participantService, probaService, registrationService, staffService));
        }

        static string GetConnectionStringByName(string name)
        {
            // Assume failure.
            string returnValue = null;

            // Look for the name in the connectionStrings section.
            ConnectionStringSettings settings = ConfigurationManager.ConnectionStrings[name];

            // If found, return the connection string.
            if (settings != null)
                returnValue = settings.ConnectionString;

            return returnValue;
        }
    }
}
