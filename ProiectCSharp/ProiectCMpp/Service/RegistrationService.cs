﻿using ProiectCMpp.Repository;
using ProiectCMpp.Service.Interface;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ProiectCMpp.Service
{
    class RegistrationService : IRegistrationService
    {
        DbRegistration _dbRegistration;
        public RegistrationService(DbRegistration dbRegistration)
        {
            _dbRegistration = dbRegistration;
        }
        public int GetParticipantCount(int idProba)
        {
            int count = _dbRegistration.GetParticipantCount(idProba);
            if (count != -1)
                return count;
            else
                throw new Exception("(1) - Eroare!");
        }

        public void RegisterParticipant(int idParticipant, int idProba)
        {
            _dbRegistration.RegisterParticipant(idParticipant, idProba);
        }
    }
}
