﻿using ProiectCMpp.Domain;
using ProiectCMpp.Repository;
using ProiectCMpp.Service.Interface;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ProiectCMpp.Service
{
    class ParticipantService : IParticipantService
    {
        DbParticipant _dbParticipant;
        public ParticipantService(DbParticipant dbParticipant)
        {
            _dbParticipant = dbParticipant;
        }

        public int AddParticipant(string firstName, string lastName, int age)
        {
            return _dbParticipant.AddParticipant(new Participant { LastName = lastName, FirstName = firstName, Age = age });
        }

        public IEnumerable<Participant> GetAll()
        {
            return _dbParticipant.GetAll();
        }

        public IEnumerable<Participant> GetAllByProba(int id)
        {
            return _dbParticipant.GetAllByProba(id);
        }

        public IEnumerable<Participant> GetParticipantByProbaId(int id)
        {
            return _dbParticipant.GetParticipantByProbaId(id);
        }
    }
}
