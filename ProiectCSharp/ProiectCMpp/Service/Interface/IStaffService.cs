﻿using ProiectCMpp.Domain;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ProiectCMpp.Service.Interface
{
    interface IStaffService
    {
        Staff FindStaffById(int id);
        Staff CheckLogin(string userName, string password);
        Staff CheckLoginWithSocket(string userName, string password);
    }
}
