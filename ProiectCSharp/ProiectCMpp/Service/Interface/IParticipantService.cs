﻿using ProiectCMpp.Domain;
using System.Collections.Generic;

namespace ProiectCMpp.Service.Interface
{
    interface IParticipantService
    {
        IEnumerable<Participant> GetAllByProba(int id);
        IEnumerable<Participant> GetAll();
        IEnumerable<Participant> GetParticipantByProbaId(int id);
        int AddParticipant(string firstName, string lastName, int age);
    }
}
