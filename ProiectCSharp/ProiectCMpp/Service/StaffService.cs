﻿using ProiectCMpp.Domain;
using ProiectCMpp.Repository;
using ProiectCMpp.Service.Interface;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ProiectCMpp.Service
{
    class StaffService : IStaffService
    {
        DbStaff _dbStaff;
        public StaffService(DbStaff dbStaff)
        {
            _dbStaff = dbStaff;
        }
        public Staff CheckLogin(string userName, string password)
        {
            string output = Utils.Utils.HashMd5(password);
           
            Staff staff = _dbStaff.FindStaff(userName, output);
            if (staff == null)
                throw new Exception("Staff was not found");
            return staff;
        }

        public Staff CheckLoginWithSocket(string userName, string password)
        {
            throw new NotImplementedException();
        }

        public Staff FindStaffById(int id)
        {
            Staff staff = _dbStaff.FindStaffById(id);
            if (staff == null)
                throw new Exception("Staff was not found");
            return staff;
        }
    }
}
