﻿using ProiectCMpp.Domain;
using ProiectCMpp.Repository;
using ProiectCMpp.Service.Interface;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ProiectCMpp.Service
{
    class ProbaService : IProbaService
    {
        DbProba _dbProba;
        public ProbaService(DbProba dbProba)
        {
            _dbProba = dbProba;
        }
        public Proba FindProba(string style, float distance)
        {
            return _dbProba.FindProba(style, distance);
        }

        public IEnumerable<Proba> GetAll()
        {
            return _dbProba.GetAll();
        }

        public IEnumerable<string> GetAllByIdParticipant(int id)
        {
            return _dbProba.GetAllByIdParticipant(id);
        }
    }
}
