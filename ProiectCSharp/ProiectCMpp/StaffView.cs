﻿using ProiectCMpp.Domain;
using ProiectCMpp.Service;
using ProiectCMpp.Utils;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Diagnostics;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace ProiectCMpp
{
    partial class StaffView : Form
    {

        ParticipantService _participantService;
        ProbaService _probaService;
        RegistrationService _registrationService;
        StaffService _staffService;
        Staff _staff;
        IDictionary<string, string> _props;
        public StaffView(IDictionary<string, string> props, ParticipantService participantService, ProbaService probaService, RegistrationService registrationService, StaffService staffService,Staff staff)
        {
            _participantService = participantService;
            _probaService = probaService;
            _registrationService = registrationService;
            _staffService = staffService;
            _staff = staff;
            _props = props;
            InitializeComponent();
        }
        class NewProba
        {
            public string Style { get; set; }
            public float Distance { get; set; }
            public int RegisteredParticipants { get; set; }
        }
        private void StaffView_Load(object sender, EventArgs e)
        {
            var source = new BindingSource();
            List<Proba> list = _probaService.GetAll().ToList();
            source.DataSource = list.Select(o => new NewProba()
            { Style = o.Style, Distance = o.Distance, RegisteredParticipants = _registrationService.GetParticipantCount(o.Id) }).ToList(); ;
            dataGrid.DataSource = source;
        }

        private void btnSearch_Click(object sender, EventArgs e)
        {
            SearchView searchView = new SearchView(_probaService, _participantService);
           // Hide();
            searchView.Show();
        }

        private void btnRegister_Click(object sender, EventArgs e)
        {
            RegisterView registerView = new RegisterView(_participantService, _probaService, _registrationService);
            //Hide();
            registerView.Show();
        }

        private void dataGrid_CellContentClick(object sender, DataGridViewCellEventArgs e)
        {

        }

        private void button1_Click(object sender, EventArgs e)
        {
            IDbConnection con = DBUtils.getConnection(_props);
            con.Close();
            Close();
        }
    }
}
