﻿using ProiectCMpp.Domain;
using ProiectCMpp.Repository.Interface;
using ProiectCMpp.Utils;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ProiectCMpp.Repository
{
    class DbStaff : IStaffRepository
    {
        IDictionary<string, string> props;
        public DbStaff(IDictionary<string, string> props)
        {
            this.props = props;
        }
        public Staff FindStaff(string user, string password)
        {
            IDbConnection con = DBUtils.getConnection(props);
            using (var comm = con.CreateCommand())
            {
                comm.CommandText = "SELECT * FROM staff where user=@user and password=@password;";

                var parameter = comm.CreateParameter();
                parameter.ParameterName = "@user";
                parameter.Value = user;
                comm.Parameters.Add(parameter);

                parameter = comm.CreateParameter();
                parameter.ParameterName = "@password";
                parameter.Value = password;
                comm.Parameters.Add(parameter);

                using (var dataR = comm.ExecuteReader())
                {
                    if (dataR.Read())
                    {
                        return GetStaff(dataR);
                    }
                }
            }
            return null;

        }
        public Staff GetStaff(IDataReader dataReader)
        {
            int id = dataReader.GetInt32(0);
            string user = dataReader.GetString(1);
            string firstName = dataReader.GetString(3);
            string lastName = dataReader.GetString(4);
            string email = dataReader.GetString(5);
            return new Staff { Id = id, User = user, Password = null, FirstName = firstName, LastName = lastName };
        }
        public Staff FindStaffById(int id)
        {
            IDbConnection con = DBUtils.getConnection(props);
            using (var comm = con.CreateCommand())
            {
                comm.CommandText = "SELECT * FROM staff where id=@id;";

                var parameter = comm.CreateParameter();
                parameter.ParameterName = "@id";
                parameter.Value = id;
                comm.Parameters.Add(parameter);

                using (var dataR = comm.ExecuteReader())
                {
                    if (dataR.Read())
                    {
                        return GetStaff(dataR);
                    }
                }
            }
            return null;
        }
    }
}
