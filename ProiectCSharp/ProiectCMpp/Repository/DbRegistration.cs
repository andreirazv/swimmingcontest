﻿using ProiectCMpp.Domain;
using ProiectCMpp.Repository.Interface;
using ProiectCMpp.Utils;
using System.Collections.Generic;
using System.Data;


namespace ProiectCMpp.Repository
{
    class DbRegistration : IRegistrationRepository
    {
        IDictionary<string, string> props;
        public DbRegistration(IDictionary<string, string> props)
        {
            this.props = props;
        }

        public int GetParticipantCount(int idProba)
        {
            IDbConnection con = DBUtils.getConnection(props);
            IList<Registration> parts = new List<Registration>();
            using (var comm = con.CreateCommand())
            {
                comm.CommandText = "SELECT count(*) FROM registration where idProba=@idProba;";

                var parameter = comm.CreateParameter();
                parameter.ParameterName = "@idProba";
                parameter.Value = idProba;
                comm.Parameters.Add(parameter);

                using (var dataR = comm.ExecuteReader())
                {
                    if (dataR.Read())
                        return dataR.GetInt32(0);
                }
            }
            return -1;
        }

        public void RegisterParticipant(int idParticipant, int idProba)
        {
            IDbConnection con = DBUtils.getConnection(props);
            IList<Registration> parts = new List<Registration>();
            using (var comm = con.CreateCommand())
            {
                comm.CommandText = "INSERT INTO registration (idParticipant,idProba) VALUES(@idParticipant, @idProba);";

                var parameter = comm.CreateParameter();
                parameter.ParameterName = "@idParticipant";
                parameter.Value = idParticipant;
                comm.Parameters.Add(parameter);

                parameter = comm.CreateParameter();
                parameter.ParameterName = "@idProba";
                parameter.Value = idProba;
                comm.Parameters.Add(parameter);

                comm.ExecuteNonQuery();
            }

        }
    }
}
