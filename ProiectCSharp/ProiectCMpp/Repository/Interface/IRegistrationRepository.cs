﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ProiectCMpp.Repository.Interface
{
    interface IRegistrationRepository
    {
        int GetParticipantCount(int idProba);
        void RegisterParticipant(int idParticipant, int idProba);
    }
}
