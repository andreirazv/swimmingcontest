﻿using ProiectCMpp.Domain;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ProiectCMpp.Repository.Interface
{
    interface IParticipantRepository
    {
        IEnumerable<Participant> GetAllByProba(int id);
        int AddParticipant(Participant participant);
        IEnumerable<Participant> GetAll();
        IEnumerable<Participant> GetParticipantByProbaId(int id);
        Participant GetParticipant(IDataReader dataReader);
    }
}
