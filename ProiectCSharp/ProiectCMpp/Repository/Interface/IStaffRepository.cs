﻿using ProiectCMpp.Domain;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ProiectCMpp.Repository.Interface
{
    interface IStaffRepository
    {
        Staff FindStaff(string user, string password);
        Staff FindStaffById(int id);
        Staff GetStaff(IDataReader dataReader);
    }
}
