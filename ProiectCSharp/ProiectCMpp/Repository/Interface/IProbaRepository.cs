﻿using ProiectCMpp.Domain;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ProiectCMpp.Repository.Interface
{
    interface IProbaRepository
    {
        IEnumerable<Proba> GetAll();
        IEnumerable<string> GetAllByIdParticipant(int id);
        Proba FindProba(string style, float distance);
    }
}
