﻿using log4net;
using ProiectCMpp.Domain;
using ProiectCMpp.Repository.Interface;
using ProiectCMpp.Utils;
using System.Collections.Generic;
using System.Data;


namespace ProiectCMpp.Repository
{
    class DbProba : IProbaRepository
    {
        private static readonly ILog log = LogManager.GetLogger("DbProba");

        IDictionary<string, string> props;
        public DbProba(IDictionary<string, string> props)
        {
            this.props = props;
        }
        public IEnumerable<Proba> GetAll()
        {
            log.InfoFormat("Entering GetAll");

            IDbConnection con = DBUtils.getConnection(props);
            IList<Proba> probs = new List<Proba>();
            using (var comm = con.CreateCommand())
            {
                comm.CommandText = "select * from proba";

                using (var dataR = comm.ExecuteReader())
                {
                    while (dataR.Read())
                    {
                        int id = dataR.GetInt32(0);
                        string style = dataR.GetString(1);
                        int distance = dataR.GetInt32(2);
                        Proba proba = new Proba { Id = id, Style = style, Distance = distance };
                        probs.Add(proba);
                    }
                }
            }

            return probs;
        }

        public IEnumerable<string> GetAllByIdParticipant(int id)
        {
            IDbConnection con = DBUtils.getConnection(props);
            IList<string> probs = new List<string>();
            using (var comm = con.CreateCommand())
            {
                comm.CommandText = "SELECT proba.id,proba.style,proba.distance from proba INNER JOIN registration where registration.idProba=proba.id and registration.idParticipant=@id;";

                var parameter = comm.CreateParameter();
                parameter.ParameterName = "@id";
                parameter.Value = id;
                comm.Parameters.Add(parameter);

                using (var dataR = comm.ExecuteReader())
                {
                    while (dataR.Read())
                    {
                        int idd = dataR.GetInt32(0);
                        string style = dataR.GetString(1);
                        int distance = dataR.GetInt32(2);
                        Proba proba = new Proba { Id = idd, Style = style, Distance = distance };
                        probs.Add(proba.Style + " - " + proba.Distance);
                    }
                }
            }

            return probs;
        }

        public Proba FindProba(string style, float distance)
        {
            IDbConnection con = DBUtils.getConnection(props);
            using (var comm = con.CreateCommand())
            {
                comm.CommandText = "SELECT * from proba where style=@style and distance=@distance;";
                var parameter = comm.CreateParameter();
                parameter.ParameterName = "@style";
                parameter.Value = style;
                comm.Parameters.Add(parameter);

                parameter = comm.CreateParameter();
                parameter.ParameterName = "@distance";
                parameter.Value = distance;
                comm.Parameters.Add(parameter);

                using (var dataR = comm.ExecuteReader())
                {
                    while (dataR.Read())
                    {
                        int idd = dataR.GetInt32(0);
                        string stylee = dataR.GetString(1);
                        int distancee = dataR.GetInt32(2);
                        return new Proba { Id = idd, Style = stylee, Distance = distancee };
                    }
                }
            }

            return null;
        }
    }
}
