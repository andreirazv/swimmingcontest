﻿using ProiectCMpp.Domain;
using ProiectCMpp.Repository.Interface;
using ProiectCMpp.Utils;
using System;
using System.Collections.Generic;
using System.Data;
using System.Diagnostics;

namespace ProiectCMpp.Repository
{
    class DbParticipant : IParticipantRepository
    {
        IDictionary<string, string> props;
        public DbParticipant(IDictionary<string, string> props)
        {
            this.props = props;
        }
        public int AddParticipant(Participant participant)
        {
            IDbConnection con = DBUtils.getConnection(props);
            IList<Participant> parts = new List<Participant>();
            using (var comm = con.CreateCommand())
            {
                comm.CommandText = "INSERT INTO participant (firstName,lastName,age) VALUES(@firstName, @lastName, @age);";

                var parameter = comm.CreateParameter();
                parameter.ParameterName = "@firstName";
                parameter.Value = participant.FirstName;
                comm.Parameters.Add(parameter);

                parameter = comm.CreateParameter();
                parameter.ParameterName = "@lastName";
                parameter.Value = participant.LastName;
                comm.Parameters.Add(parameter);

                parameter = comm.CreateParameter();
                parameter.ParameterName = "@age";
                parameter.Value = participant.Age;
                comm.Parameters.Add(parameter);
                comm.ExecuteNonQuery();

                comm.CommandText = "SELECT LAST_INSERT_ID();";
                return Convert.ToInt32(comm.ExecuteScalar());
            }
        }

        public IEnumerable<Participant> GetAll()
        {
            IDbConnection con = DBUtils.getConnection(props);
            IList<Participant> parts = new List<Participant>();
            using (var comm = con.CreateCommand())
            {
                comm.CommandText = "SELECT * FROM participant;";

                using (var dataR = comm.ExecuteReader())
                {
                    while (dataR.Read())
                    {
                        parts.Add(GetParticipant(dataR));
                    }
                }
            }

            return parts;
        }
        public Participant GetParticipant(IDataReader dataReader)
        {
            int idd = dataReader.GetInt32(0);
            string firstName = dataReader.GetString(1);
            string lastName = dataReader.GetString(2);
            int age = dataReader.GetInt32(3);
            return new Participant { Id = idd, FirstName = firstName, LastName = lastName, Age = age };
        }
        public IEnumerable<Participant> GetAllByProba(int id)
        {
            IDbConnection con = DBUtils.getConnection(props);
            IList<Participant> parts = new List<Participant>();
            using (var comm = con.CreateCommand())
            {
                comm.CommandText = "SELECT participant.id,participant.firstName,participant.lastName,participant.age FROM participant inner join registration ON participant.id = registration.idParticipant and registration.idProba=@id;";

                var parameter = comm.CreateParameter();
                parameter.ParameterName = "@id";
                parameter.Value = id;
                comm.Parameters.Add(parameter);

                using (var dataR = comm.ExecuteReader())
                {
                    while (dataR.Read())
                    {
                        parts.Add(GetParticipant(dataR));
                    }
                }
            }

            return parts;
        }

        public IEnumerable<Participant> GetParticipantByProbaId(int id)
        {
            IDbConnection con = DBUtils.getConnection(props);
            IList<Participant> parts = new List<Participant>();
            using (var comm = con.CreateCommand())
            {
                comm.CommandText = "SELECT participant.id,participant.firstName,participant.lastName,participant.age FROM participant inner join registration ON participant.id = registration.idParticipant and registration.idProba=@id;";

                var parameter = comm.CreateParameter();
                parameter.ParameterName = "@id";
                parameter.Value = id;
                comm.Parameters.Add(parameter);

                using (var dataR = comm.ExecuteReader())
                {
                    while (dataR.Read())
                    {
                        parts.Add(GetParticipant(dataR));
                    }
                }
            }

            return parts;
        }
    }
}
