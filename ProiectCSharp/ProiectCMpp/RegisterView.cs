﻿using ProiectCMpp.Domain;
using ProiectCMpp.Service;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace ProiectCMpp
{
    partial class RegisterView : Form
    {
        ParticipantService _participantService;
        ProbaService _probaService;
        RegistrationService _registrationService;
        public RegisterView(ParticipantService participantService, ProbaService probaService, RegistrationService registrationService)
        {
            _participantService = participantService;
            _probaService = probaService;
            _registrationService = registrationService;
            InitializeComponent();
        }
        private List<int> selectedProba=new List<int>();
        private void textBoxAge_TextChanged(object sender, EventArgs e)
        {

        }

        private void btnRegister_Click(object sender, EventArgs e)
        {
            string firstName = textBoxFirstName.Text;
            string lastName = textBoxLastName.Text;
            string ageText = textBoxAge.Text;
            if(!firstName.Equals("")&&!lastName.Equals("")&&!textBoxAge.Equals("")&&selectedProba.Count!=0)
            {
                int age = int.Parse(ageText);
                int id = _participantService.AddParticipant(firstName, lastName, age);
                selectedProba.ForEach(idProba =>
                {
                    _registrationService.RegisterParticipant(id, idProba);
                });
                MessageBox.Show("Participant was registered!");
                textBoxAge.Clear();
                textBoxLastName.Clear();
                textBoxFirstName.Clear();
                checkListProba.ClearSelected();
            }
            else
            {
                MessageBox.Show("Please complete all data!");
            }
        }

        private void RegisterView_Load(object sender, EventArgs e)
        {    
            foreach (Proba proba in _probaService.GetAll().ToList())
            {
                checkListProba.Items.Add(proba.Style + "-" + proba.Distance);
            }
        }

        private void SelectProba(object sender, ItemCheckEventArgs e)
        {
            string[] str = ((string)checkListProba.Items[e.Index]).Split('-');
            Proba proba = _probaService.FindProba(str[0], float.Parse(str[1]));
            if (proba != null)
            {
                int id = proba.Id;
                if (e.NewValue.ToString().Equals("Checked"))
                {
                    selectedProba.Add(id);
                }
                else
                {
                    selectedProba.Remove(id);
                }
            }
        }
    }
}
