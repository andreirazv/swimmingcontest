﻿using MySql.Data.MySqlClient;
using System;
using System.Collections.Generic;
using System.Data;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ProiectCMpp
{
    public class MySqlConnectionFactory:ConnectionFactory
	{
		public override IDbConnection createConnection(IDictionary<string,string> props)
		{
			string connectionString = props["ConnectionString"];
			Debug.WriteLine("MySql new connection {0}", connectionString);
			
			return new MySqlConnection(connectionString);

	
		}
	}
}
