﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ProiectCMpp.Domain
{
    interface IHasID
    {
        int Id { get; set; }
    }
}
