﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ProiectCMpp.Domain
{
    class Registration : IHasID
    {
        public int Id { get; set; }
        public int IdParticipant { get; set; }
        public int IdProba { get; set; }
    }
}
