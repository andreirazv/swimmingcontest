﻿using ProiectCMpp.Domain;
using ProiectCMpp.Service;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace ProiectCMpp
{
    partial class SearchView : Form
    {
        ProbaService _probaService;
        ParticipantService _participantService;
        public SearchView(ProbaService probaService, ParticipantService participantService)
        {
            _probaService = probaService;
            _participantService = participantService;
            InitializeComponent();
        }
        class NewParticipant
        {
            public string FirstName { get; set; }
            public string LastName { get; set; }
            public int Age { get; set; }
        }

        private void InitFilterComboBox()
        {
            comboBoxProba.Items.Add("No selection");
            _probaService.GetAll().ToList().ForEach(x =>
            {
                comboBoxProba.Items.Add(x.Style + "-" + x.Distance);
            });
        }

        private void SearchView_Load(object sender, EventArgs e)
        {
            InitFilterComboBox();
            UpdateTable(false,-1);

        }
        private void UpdateTable(bool selected, int id)
        {
            dataGridParticipant.Rows.Clear();
            dataGridParticipant.Columns.Clear();
            dataGridParticipant.Refresh();
            
            List<Participant> currentSelection;
            if (!selected)
            {
                currentSelection = _participantService.GetAll().ToList();
            }
            else
            {
                currentSelection = _participantService.GetParticipantByProbaId(id).ToList();
            }

            var source = new BindingSource();
            List<Participant> list = currentSelection;
            source.DataSource = list.Select(x =>

                new NewParticipant()
                {
                    FirstName = x.FirstName,
                    LastName = x.LastName,
                    Age = x.Age
                }).ToList();

            dataGridParticipant.DataSource = source;

            var column = new DataGridViewComboBoxColumn();
            column.Name = "Registered Proba";
            dataGridParticipant.Columns.Add(column);

            int i = 0;
            int size = currentSelection.Count;
            foreach (DataGridViewRow row in dataGridParticipant.Rows)
            {
                if (i == size)
                    break;
                DataGridViewComboBoxCell cell = (DataGridViewComboBoxCell)(row.Cells["Registered Proba"]);

                foreach (string part in _probaService.GetAllByIdParticipant(currentSelection.ElementAt(i).Id))
                {
                    cell.Items.Add(part);
                }
                i++;
            }
        }

        private void OnTextChangedEvent(object sender, EventArgs e)
        {
            string text = comboBoxProba.Text;
            if(text!=null)
            {
                if(text.Equals("No selection"))
                {
                    UpdateTable(false,-1);
                }
                else
                {
                    string[] split = text.Split('-');
                    float distance = float.Parse(split[1]);
                    int id = _probaService.FindProba(split[0], distance).Id;
                    UpdateTable(true,id);
                }
            }
        }
    }
}
