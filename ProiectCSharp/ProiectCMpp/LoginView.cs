﻿using ProiectCMpp.Domain;
using ProiectCMpp.Service;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace ProiectCMpp
{
    partial class LoginView : Form
    {
        ParticipantService _participantService;
        ProbaService _probaService;
        RegistrationService _registrationService;
        StaffService _staffService;
        IDictionary<string, string> _props;
        public LoginView(IDictionary<string, string> props,ParticipantService participantService,ProbaService probaService, RegistrationService registrationService, StaffService staffService)
        {
            _participantService = participantService;
            _probaService = probaService;
            _registrationService = registrationService;
            _staffService = staffService;
            _props = props;
            InitializeComponent();
        }

        private void btnLogin_Click(object sender, EventArgs e)
        {
            try
            {
                string userName = textBoxUser.Text;
                string password = textBoxPassword.Text;
                if (!userName.Equals("") && !password.Equals(""))
                {
                    Staff staff = _staffService.CheckLogin(userName, password);
                    if (staff != null)
                    {
                        StaffView staffView = new StaffView(_props,_participantService, _probaService, _registrationService, _staffService, staff);
                        //this.Hide();
                        staffView.Show();

                    }
                    else
                    {
                        MessageBox.Show("User or password is wrong!");
                    }

                }
            }
            catch(Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }
    }
}
